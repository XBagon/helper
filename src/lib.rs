pub use derive_more;
pub use derivative;

#[macro_export]
macro_rules! exit {
    () => (eprintln!("Error"));
    ($($arg:tt)*) => ({
        eprint!("Error: ");
        eprintln!($($arg)*);
        std::process::exit(1);
    })
}

